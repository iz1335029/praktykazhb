import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
from matplotlib.backends.backend_pdf import PdfPages
from datetime import date

Path = 'X:\ZHB\Kacper Zukowski\Adrian\data\wyniki'
atr_order_df = pd.read_excel('X:\ZHB\Kacper Zukowski\Adrian\data\KODY_CECH_INTERBULL.xlsx', header=None, index_col=0)
year = str(date.today().year)


def file_list(path):
    clear_file_list = []
    folderlist = os.listdir(Path)  # zapisanie listy folderów
    folderlist.sort(reverse=True)
    for folder in folderlist:
        if 'wyniki_EG_' in folder and int(folder[-4:-2]) > 22:#(int(year[-2:]) - 5):
            filelist = os.listdir(path + '/' + folder)
            for file in filelist:
                if 'GP400' in file:
                    clear_file_list.append(path + '/' + folder + '/' + file)
    return clear_file_list


def create_df(file_list):
    frame = pd.DataFrame(columns=['Attribute', 'Year', 'Series', 'Breed', 'Country', 'ID'])
    for file in file_list:
        print(file)
        rawframe = []
        filename = file.split('/')[5]
        atr = filename.split('.')[1]
        series = filename.split('.')[2]
        year = series[0:2]
        series = series[2:]
        with open(file) as f:
            list = [row.split()[0] for row in f]
        for a in list:
            breed = a[0:3]
            country = a[3:6]
            cow_id = a[6:]
            rawframe.append(
                {
                    'Attribute': atr,
                    'Year': '20' + year,
                    'Series': series,
                    'Breed': breed,
                    'Country': country,
                    'ID': cow_id
                }
            )
        rawdf = pd.DataFrame(rawframe)
        frame = frame._append(rawdf, ignore_index=True)
    frame = frame.drop_duplicates()
    frame = frame.groupby(['Attribute', 'Year', 'Series', 'Breed', 'Country'], as_index=False).count()
    frame = pd.pivot_table(frame, index=['Attribute', 'Year', 'Series', 'Breed', 'Country'], values=['ID'])
    frame = frame.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0)
    return frame


def series_plot(frame):
    for year_index in frame.index.unique(level='Year'):
        for series in frame.index.unique(level='Series'):
            plt.figure(series)
            print(frame)
            frame2 = frame[frame.index.isin([year_index], level=1)]
            frame2 = frame2[frame2.index.isin([series], level=2)]
            if frame2.empty == False:
                xaxis_values = frame.index.get_level_values(0).unique().tolist()
                yplot = frame2.groupby(level=0)['ID'].sum()
                yplot = yplot.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0)
                plotpalette = ['yellow' if x in xaxis_values[0:4] else 'lightblue' if x in xaxis_values[4:28] else 'teal' if x in xaxis_values[28] else 'lime' if x in xaxis_values[29] else 'chocolate' if x in xaxis_values[30:36] else 'darkorchid' if x in xaxis_values[36:40] else 'bisque' for x in xaxis_values]
                xaxis_length = [i for i in range(len(xaxis_values))]
                plt.subplots(layout='constrained')
                plt.figure(figsize=(20, 9), dpi=120)
                figure = sns.barplot(x=xaxis_values, y=yplot, errorbar=None, palette=plotpalette)
                figure.set(ylabel=None, xlabel=None, title=(year_index + ' ' + series))
                atr_labels = atr_order_df[atr_order_df.columns[0]].values.tolist()
                plt.xticks(ticks=xaxis_length, labels=atr_labels, rotation=90)
                #plt.savefig('barplot_' + series + '.png', bbox_inches='tight')
                pdf_pages.savefig(bbox_inches='tight')
                plt.close()


def comparative_plot(frame):
    comp_frame = frame.groupby(['Attribute', 'Year', 'Series'], as_index=True).sum()
    comp_frame = comp_frame.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0)
    print(comp_frame)
    for index_year in frame.index.unique(level='Year'):
        for index_series in frame.index.unique(level='Series'):
            plt.figure(index_year)
            print(index_year,index_series)
            if index_series == '12':  #and int(index_year)+1 in comp_frame.index.get_level_values(1):
                base_df = comp_frame[comp_frame.index.isin([index_year], level=1)]
                base_df = base_df[base_df.index.isin([index_series], level=2)]
                base_df = base_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                comp_df = comp_frame[comp_frame.index.isin([index_year], level=1)]
                comp_df = comp_df[comp_df.index.isin(['08'], level=2)]
                comp_df = comp_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                label = str(index_year) + ', 12 - ' + str(index_year) + ', 8'
            elif index_series == '08':
                base_df = comp_frame[comp_frame.index.isin([index_year], level=1)]
                base_df = base_df[base_df.index.isin([index_series], level=2)]
                base_df = base_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                comp_df = comp_frame[comp_frame.index.isin([index_year], level=1)]
                comp_df = comp_df[comp_df.index.isin(['04'], level=2)]
                comp_df = comp_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                label = str(index_year) + ', 8 - ' + str(index_year) + ', 4'
            elif index_series == '04':  #and int(index_year)-1 in comp_frame.index.get_level_values(1):
                base_df = comp_frame[comp_frame.index.isin([index_year], level=1)]
                base_df = base_df[base_df.index.isin([index_series], level=2)]
                base_df = base_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                comp_df = comp_frame[comp_frame.index.isin([str(int(index_year)-1)], level=1)]
                comp_df = comp_df[comp_df.index.isin(['12'], level=2)]
                comp_df = comp_df.reindex(atr_order_df.index.get_level_values(0).unique().tolist(), level=0, fill_value=0)
                label = str(index_year) + ', 4 - ' + str(int(index_year)-1) + ', 12'
            else:
                continue
            if comp_df.empty or base_df.empty:
                continue
            print(base_df)
            print(comp_df)
            comp_df_index = comp_df.index.get_level_values(0).unique()
            base_df_index = base_df.index.get_level_values(0).unique()
            if len(base_df) < len(comp_df):
                comp_df = comp_df[comp_df_index.isin(base_df_index)]
            if len(base_df) > len(comp_df):
                base_df = base_df[base_df_index.isin(comp_df_index)]
            comp_df_id = comp_df['ID'].tolist()
            base_df.loc[:, 'comp'] = comp_df_id
            print(base_df)
            yaxis_values = np.where(base_df['ID'] == base_df['comp'], 0, base_df['ID'] - base_df['comp'])
            xaxis_values = base_df.index.get_level_values(0).unique().tolist()
            xaxis_length = [i for i in range(len(yaxis_values))]
            plt.subplots(layout='constrained')
            plt.figure(figsize=(20, 9), dpi=120)
            plotpalette = ['yellow' if x in xaxis_values[0:4] else 'lightblue' if x in xaxis_values[4:28] else 'teal' if x in xaxis_values[28] else 'lime' if x in xaxis_values[29] else 'chocolate' if x in xaxis_values[30:36] else 'darkorchid' if x in xaxis_values[36:40] else 'bisque' for x in xaxis_values]
            print(len(xaxis_values))
            print(len(yaxis_values))
            figure = sns.barplot(x=xaxis_values, y=yaxis_values, palette=plotpalette)
            figure.set(ylabel=None, xlabel=None, title=(label))
            atr_labels = atr_order_df[atr_order_df.columns[0]].values.tolist()
            plt.xticks(ticks=xaxis_length, labels=atr_labels[:len(xaxis_length)], rotation=90)
            for i in figure.containers:
                figure.bar_label(i, )
            #plt.savefig('barplot_comp_' + label + '.png', bbox_inches='tight')
            pdf_pages.savefig(bbox_inches='tight')
            plt.close()


with PdfPages('wyniki'+ year + '.pdf') as pdf_pages:
    filelist = file_list(Path)
    print(filelist)
    clean_frame = create_df(filelist)
    series_plot(clean_frame)
    comparative_plot(clean_frame)
